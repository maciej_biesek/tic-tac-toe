(function() {

	function showStartDialog(title) {
		$("#start-dialog").dialog({
			buttons: {
				"Tak": function() {
					var figure = $("#start-dialog").find("input[name='figure']:checked")[0].id;
					startGame(figure, true);

					$(this).dialog("close");
				},
				"Nie": function() {
					var figure = $("#start-dialog").find("input[name='figure']:checked")[0].id;
					startGame(figure, false);

					$(this).dialog("close");
				}
			}
		});
	}

	function showEndingDialog(title) {
		$("<div></div>").dialog({
			title: title,
			open: function() {
				var markup = "Chcesz zagrać jeszcze raz?";
				$(this).html(markup);
			},
			buttons: {
				"Tak": function() {
					window.location.reload();
					$(this).dialog("close");
				},
				"Nie": function() {
					$(this).dialog("close");
				}
			}
		});
	}

	function updateBoard(board) {
		for (var i = 0; i < board.length; i++) {
			for (var j = 0; j < board[i].length; j++) {
				if (board[i][j] === "x") {
					$("div[id='" + i + " " + j + "']")[0].classList.add("cross");
				} else if (board[i][j] === "o") {
					$("div[id='" + i + " " + j + "']")[0].classList.add("circle");
				}
			}
		}
	}

	function checkIfDraw(board) {
		for (var i = 0; i < board.length; i++) {
			var containNull = board[i].some(function(item) { return item === null });
		    if(containNull) {
				return false;
			}
		}
		return true;
	}

	function startGame(figure, playerFirst) {
		$.ajax({
			url: "http://interview.314.tt/api/game",
			type: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			contentType: "application/json",
			data: JSON.stringify({
				"player": figure,
				"playerStarts": playerFirst
			}),

			success: function(data) {
				localStorage.setItem('token', data.token);
				$("#board").show();
				updateBoard(data.game.board);
			},

			error: function(data) {
				console.log(data);
			}
		});
	}

	function drawMatchLine(points) {
		var boardHeight = $("#board").height();
		var boardPosition =  $("#board").position();
		$("#board_canvas").css("top", boardPosition.top)
						  .css("left", boardPosition.left);
		var canvas = $("#board_canvas")[0];
		canvas.setAttribute("width", boardHeight);
		canvas.setAttribute("height", boardHeight);
		var ctx = canvas.getContext("2d");
		ctx.strokeStyle = "red";
		ctx.lineWidth = 10;

		console.log(points)
		ctx.beginPath();
		ctx.moveTo(points[0][1] * 100 + 50, points[0][0] * 100 + 50);
		ctx.lineTo(points[2][1] * 100 + 50 , points[2][0] * 100 + 50);
		ctx.stroke();
	}

	function makeMove(x, y) {
		$.ajax({
			url: "http://interview.314.tt/api/game",
			type: "PUT",
			headers: {
				"Content-Type": "application/json",
				"token": localStorage.getItem('token')
			},
			contentType: "application/json",
			data: JSON.stringify({
				"x": x,
				"y": y
			}),

			success: function(data) {
				updateBoard(data.game.board);

				if (checkIfDraw(data.game.board)) {
					showEndingDialog("Remis!");
				} else if (data.game.winner) {
					var match = data.game.winner.match;
					drawMatchLine(match);
					if (data.game.winner.figure === data.game.player) {
						showEndingDialog("Wygrałeś!");
					} else {
						showEndingDialog("Przegrałeś!");
					}
				}
			},

			error: function(data) {
				var obj = JSON.parse(data.responseText);
				alert(obj.message);
			}
		});
	}

	window.onload = function() {
		showStartDialog();
		$(".field").click(function(event) {
			var splittedId =  this.id.split(" ");
			var x = splittedId[0];
			var y = splittedId[1];
			makeMove(x, y)
		});
	};

})();
